#include "dataStream.hpp"

DataStream::DataStream():
    mReadPosition(0),
    mIsValid(true) {
    ;
}

void DataStream::append(const void* data, const std::size_t size) {
    if(data != nullptr && size > 0) {
        std::size_t start = mData.size();
        mData.resize(start + size);
        std::memcpy(&mData.at(start), data, size);
    }
}

void DataStream::clear() {
    mData.clear();
    mReadPosition = 0;
    mIsValid = true;
}

const void* DataStream::getData() const {
    return !mData.empty() ? mData.data() : nullptr;
}

const std::size_t DataStream::getDataSize() const {
    return mData.size();
}

const bool DataStream::endOfData() const {
    return mReadPosition >= mData.size();
}

const bool DataStream::loadFromFile(const std::string& path) {
    mData.clear();
    std::ifstream stream(path, std::ios::in | std::ios::binary | std::ios::ate);
    if(stream.is_open()) {
        mData.resize(stream.tellg());
        stream.seekg(0);
        stream.read(mData.data(), mData.size());
        return true;
    }
    return false;
}

const bool DataStream::saveToFile(const std::string& path) const {
    std::ofstream stream(path, std::ios::out | std::ios::binary | std::ios::trunc);
    if(stream.is_open()) {
        stream.write(mData.data(), mData.size());
        return true;
    }
    return false;
}

const bool DataStream::appendToFile(const std::string& path) const {
    std::ofstream stream(path, std::ios::out | std::ios::binary | std::ios::app);
    if(!stream.is_open()) {
        stream.open(path, std::ios::out | std::ios::binary | std::ios::trunc);
    }
    if(stream.is_open()) {
        stream.write(mData.data(), mData.size());
        return true;
    }
    return false;
}

DataStream::operator bool() const {
    return mIsValid;
}

DataStream& DataStream::operator >> (bool& data) {
    std::int8_t value;
    if(*this >> value) {
        data = (value != 0);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::int8_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::int8_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::uint8_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::uint8_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::int16_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::int16_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::uint16_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::uint16_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::int32_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::int32_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::uint32_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::uint32_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::int64_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::int64_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (std::uint64_t& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const std::uint64_t*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (float& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const float*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (double& data) {
    if(checkSize(sizeof(data))) {
        data = *reinterpret_cast<const double*>(&mData.at(mReadPosition));
        mReadPosition += sizeof(data);
    }
    return *this;
}

DataStream& DataStream::operator >> (char* data) {
    std::uint32_t length = 0;
    *this >> length;

    if(length > 0 && checkSize(length)) {
        std::memcpy(data, &mData.at(mReadPosition), length);
        data[length] = '\0';
        mReadPosition += length;
    }
    return *this;
}

DataStream& DataStream::operator >> (std::string& data) {
    std::uint32_t length = 0;
    *this >> length;

    data.clear();
    if(length > 0 && checkSize(length)) {
        data.assign(&mData.at(mReadPosition), length);
        mReadPosition += length;
    }
    return *this;
}

DataStream& DataStream::operator >> (wchar_t* data) {
    std::uint32_t length = 0;
    *this >> length;

    if(length > 0 && checkSize(length * sizeof(std::uint32_t))) {
        for(std::uint32_t i = 0; i < length; ++i) {
            std::uint32_t value = 0;
            *this >> value;
            data[i] = static_cast<wchar_t>(value);
        }
        data[length] = L'\0';
    }
    return *this;
}

DataStream& DataStream::operator >> (std::wstring& data) {
    std::uint32_t length = 0;
    *this >> length;

    data.clear();
    if(length > 0 && checkSize(length * sizeof(std::uint32_t))) {
        for(std::uint32_t i = 0; i < length; ++i) {
            std::uint32_t value = 0;
            *this >> value;
            data += static_cast<wchar_t>(value);
        }
    }
    return *this;
}


DataStream& DataStream::operator << (const bool data) {
    *this << static_cast<const std::uint8_t>(data);
    return *this;
}

DataStream& DataStream::operator << (const std::int8_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::uint8_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::int16_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::uint16_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::int32_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::uint32_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::int64_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const std::uint64_t data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const float data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const double data) {
    append(&data, sizeof(data));
    return *this;
}

DataStream& DataStream::operator << (const char* data) {
    std::uint32_t length = static_cast<std::uint32_t>(strlen(data));
    *this << length;

    append(data, length * sizeof(char));
    return *this;
}

DataStream& DataStream::operator << (const std::string& data) {
    std::uint32_t length = static_cast<std::uint32_t>(data.size());
    *this << length;

    if(length > 0) {
        append(data.c_str(), length * sizeof(std::string::value_type));
    }
    return *this;
}

DataStream& DataStream::operator << (const wchar_t* data) {
    std::uint32_t length = static_cast<std::uint32_t>(std::wcslen(data));
    *this << length;

    if(length > 0) {
        for(const wchar_t* value = data; value != L'\0'; ++value) {
            *this << static_cast<std::uint32_t>(*value);
        }
    }
    return *this;
}

DataStream& DataStream::operator << (const std::wstring& data) {
    std::uint32_t length = static_cast<std::uint32_t>(data.size());
    *this << length;

    for(std::uint32_t i = 0; i < length; ++i) {
        *this << static_cast<std::uint32_t>(data.at(i));
    }
    return *this;
}


const bool DataStream::checkSize(const std::size_t size) {
    return mIsValid = mIsValid && (mReadPosition + size <= mData.size());
}
