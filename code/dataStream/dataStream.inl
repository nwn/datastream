
template <typename T>
DataStream& operator >> (DataStream& stream, std::vector<T>& data) {
    std::uint32_t size = 0;
    stream >> size;

    data.resize(size);
    for(std::uint32_t i = 0; i < size; ++i) {
        stream >> data.at(i);
    }
    return stream;
}

template <typename T>
DataStream& operator << (DataStream& stream, const std::vector<T>& data) {
    std::uint32_t size = static_cast<std::uint32_t>(data.size());
    stream << size;

    for(std::uint32_t i = 0; i < size; ++i) {
        stream << data.at(i);
    }
    return stream;
}
