#ifndef HG_DATASTREAM_HPP
#define HG_DATASTREAM_HPP

#include <fstream>
#include <vector>
#include <cstring>
#include <cstdint>

class DataStream {
private:
    std::vector<char> mData;
    std::size_t mReadPosition;
    bool mIsValid;

public:
    DataStream();

    void append(const void* data, const std::size_t size);
    void clear();
    const void* getData() const;
    const std::size_t getDataSize() const;
    const bool endOfData() const;

    const bool loadFromFile(const std::string& path);
    const bool saveToFile(const std::string& path) const;
    const bool appendToFile(const std::string& path) const;

    explicit operator bool() const;

    DataStream& operator >> (bool& data);
    DataStream& operator >> (std::int8_t& data);
    DataStream& operator >> (std::uint8_t& data);
    DataStream& operator >> (std::int16_t& data);
    DataStream& operator >> (std::uint16_t& data);
    DataStream& operator >> (std::int32_t& data);
    DataStream& operator >> (std::uint32_t& data);
    DataStream& operator >> (std::int64_t& data);
    DataStream& operator >> (std::uint64_t& data);
    DataStream& operator >> (float& data);
    DataStream& operator >> (double& data);
    DataStream& operator >> (char* data);
    DataStream& operator >> (std::string& data);
    DataStream& operator >> (wchar_t* data);
    DataStream& operator >> (std::wstring& data);

    DataStream& operator << (const bool data);
    DataStream& operator << (const std::int8_t data);
    DataStream& operator << (const std::uint8_t data);
    DataStream& operator << (const std::int16_t data);
    DataStream& operator << (const std::uint16_t data);
    DataStream& operator << (const std::int32_t data);
    DataStream& operator << (const std::uint32_t data);
    DataStream& operator << (const std::int64_t data);
    DataStream& operator << (const std::uint64_t data);
    DataStream& operator << (const float data);
    DataStream& operator << (const double data);
    DataStream& operator << (const char* data);
    DataStream& operator << (const std::string& data);
    DataStream& operator << (const wchar_t* data);
    DataStream& operator << (const std::wstring& data);

private:
    const bool checkSize(const std::size_t size);
};

template <typename T>
DataStream& operator >> (DataStream& stream, std::vector<T>& data);
template <typename T>
DataStream& operator << (DataStream& stream, const std::vector<T>& data);

#include "dataStream.inl"

#endif // HG_DATASTREAM_HPP
