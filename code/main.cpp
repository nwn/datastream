#include <iostream>

#include "dataStream/dataStream.hpp"

int main() {

    DataStream stream;
    stream << 12 << "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
    if(!stream) {
        std::cout << "Failed to input\n";
    } else {
        std::cout << "Stream size in bytes: " << stream.getDataSize() << std::endl;
    }

    if(!stream.saveToFile("dataStream.test")) {
        std::cout << "Failed to save\n";
    }
    stream.clear();
    if(!stream.loadFromFile("dataStream.test")) {
        std::cout << "Failed to load\n";
    }

    std::int32_t iValue;
    std::string sValue;
    if(!(stream >> iValue >> sValue)) {
        std::cout << "Failed to output\n";
    }
    std::cout << iValue << ", " << sValue << std::endl;

    return 0;
}
